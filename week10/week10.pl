use strict;
use warnings;

my $file_name = $ARGV[0];

open FILE , '<', $file_name or die "not open $file_name\n";

my @lines = <FILE>;

close FILE;

foreach my $l (@lines) {
    chomp $l;    
    
    if($l =~ /(.*) \- \- \[(.*) \-(.*)\] \"(.*) (.*) HTTP\/(.*)\" (\d{3}) (.*)/){
            
            print "$1 $7    ";
            if($7 <= 300){ print "success\n"}
            elsif($7 < 400){"redirect\n"}
            elsif($7 < 500){"client error\n"}
            else{ print "server error\n"}
     }

}

